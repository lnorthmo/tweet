create table if not exists users
(
    id          serial
        constraint changes_pk
            primary key,
    user_name   varchar,
    user_id     varchar,
    link        varchar,
    description varchar,
    followers   integer,
    following   integer
);

alter table users
    owner to postgres;

create unique index if not exists users_id_uindex
    on users (id);

create unique index if not exists users_user_id_uindex
    on users (user_id);



create unique index if not exists users_id_uindex
    on users (id);

create table tweets
(
    id       serial
        constraint tweets_pk
            primary key,
    user_id  varchar not null,
    tweet_id int     not null,
    text     varchar
);

create unique index tweets_id_uindex
    on tweets (id);

create unique index tweets_tweet_id_uindex
    on tweets (tweet_id);