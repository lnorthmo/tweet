from fastapi import APIRouter

from services import tweet_service
from schemas.tweet_schema import UserList
from worker import create_task

router = APIRouter()


@router.post("/api/add_users", status_code=201)
async def api_get_spreads(users: UserList):
    create_task.delay(users.json())
    return {'status': "users accepted"}


@router.get('/api/get_user_tweets')
async def get_users_tweets(user_id: str):
    return await tweet_service.get_users_tweets(user_id)


@router.get('/api/get_users')
async def get_users():
    return await tweet_service.get_users()
