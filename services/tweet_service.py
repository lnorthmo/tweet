import json
import os

import aiohttp

from dotenv import load_dotenv
from schemas.tweet_schema import UserList, User, Tweet
from services.managers import pg_manager

load_dotenv()

API_BEARER_TOKEN = os.getenv("API_BEARER_TOKEN")

# API_BEARER_TOKEN = 'AAAAAAAAAAAAAAAAAAAAADFbbgEAAAAAgbDQ72%2BKSSj4LUlfXndxsYyAs1c%3D7rYx14Ozbpzl7dJF4DkCsWxj198YKHM60emOl7bDtZhO24TQ4h'


async def get_users_info(users: str) -> list:
    users = json.loads(users)
    users = [user.replace('https://twitter.com/', '') for user in users['users']]

    d = 0
    k = 100
    ids = []
    fin_res = []

    headers = {
        "Authorization": f"Bearer {API_BEARER_TOKEN}",
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36'
                      ' (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36'
    }

    while d < len(users):

        search_string = ''
        for user in users[d:k]:
            search_string += user + ','
        d += 100
        k += 100

        search_string = search_string[:-1]
        async with aiohttp.ClientSession() as session:
            result = await session.get('https://api.twitter.com/2/users/by', headers=headers,
                                       params={
                                           'user.fields': 'public_metrics,description',
                                           'usernames': search_string
                                       })
            res = await result.json()
            res = res['data']
            print(res)
            for user in res:
                fin_res.append(('https://twitter.com/' + user['username'], user['id'], user['username'],
                                user['description'], user['public_metrics']['followers_count'],
                                user['public_metrics']['following_count']))
                ids.append(user['id'])
    await pg_manager.insert_users(fin_res)

    return ids


async def get_and_insert_tweets(users_ids: list):
    headers = {
        "Authorization": f"Bearer {API_BEARER_TOKEN}",
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36'
                      ' (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36'
    }
    users_tweets = []
    async with aiohttp.ClientSession() as session:
        for user_id in users_ids:
            result = await session.get(f'https://api.twitter.com/2/users/{user_id}/tweets', headers=headers,
                                       params={
                                           'max_results': 10

                                       })
            res = await result.json()
            res = res['data']
            for tw in res:
                users_tweets.append((user_id, tw['id'], tw['text']))
    await pg_manager.insert_tweets(users_tweets)


async def get_users_tweets(user_id: str) -> dict:
    data = await pg_manager.get_users_tweets(user_id)
    resp = {'user_id': user_id,
            'tweets': []}
    for tw in data:
        resp['tweets'].append(Tweet(**tw))
    return resp


async def get_users():
    data = await pg_manager.get_users()
    res = {'users': []}
    for u in data:
        res['users'].append(User(**u))
    return res
