import asyncpg

from settings import PostgreSqlConfig


async def connect():
    conn = await asyncpg.connect(PostgreSqlConfig.DATABASE_URL)
    return conn


async def insert_users(data: list):
    cur = await connect()

    await cur.executemany('insert into users (link, user_id, user_name, description, followers, following)'
                          'values ($1,$2,$3, $4, $5, $6) on conflict do nothing', data)

    await cur.close()


async def get_users_tweets(user_id: str):
    cur = await connect()

    data = await cur.fetch("select tweet_id, text from tweets where user_id = $1", user_id)
    await cur.close()
    return data


async def get_users():
    cur = await connect()

    data = await cur.fetch('select * from users')
    await cur.close()
    return data


async def insert_tweets(user_tweets: list):
    cur = await connect()

    await cur.executemany('insert into tweets (user_id, tweet_id,text) values ($1, $2, $3) on conflict do nothing',
                          user_tweets)

    await cur.close()
