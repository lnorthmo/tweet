from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from routes import tweet


app = FastAPI()
# app = FastAPI(docs_url=None, redoc_url=None, openapi_url=None)
# app.openapi = None

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(tweet.router)
