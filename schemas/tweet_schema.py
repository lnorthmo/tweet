from pydantic import BaseModel
from typing import Optional


class User(BaseModel):
    id: Optional[int]
    link: str
    user_id: str
    user_name: str
    description: Optional[str]
    followers: int
    following: int


class UserList(BaseModel):
    users: list


class Tweet(BaseModel):
    user_id: Optional[str]
    tweet_id: str
    text: str
