import os
import asyncio

from celery import Celery
from services import tweet_service


celery = Celery(__name__)
celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://redis:6379")
celery.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://redis:6379")


@celery.task(name='create_task')
def create_task(users):
    loop = asyncio.get_event_loop()
    coro = users_pars(users)
    loop.run_until_complete(coro)


async def users_pars(users):
    data = await tweet_service.get_users_info(users)
    await tweet_service.get_and_insert_tweets(data)
